#!/usr/bin/python

import math
import numpy as np
import csv 
import scipy.integrate as spi
#import scipy.integrate as spi#.integrate as integ
import os
from subprocess import call
import subprocess
from os import listdir
from os.path import isfile, join, isdir
import fnmatch
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
#from scipy.signal import argrelextrema
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
#import colormaps as cmaps
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import scipy.linalg as sli
import scipy.sparse as spa
from mpi4py import MPI
import time
import json as js


import src.basics as bsc
import src.pulses as pul

############# MPI init ###############################################
comm = MPI.COMM_WORLD
irank = comm.Get_rank()
nsize = comm.Get_size()
############################################################################

############################################################################
if irank== 0 :
  time1 =time.time()



inpdir="input/"
runname="LF"
outdir="output_"+runname+"/"

if irank== 0 :
  bsc.directory_check_create(outdir,clear=True)

comm.Barrier()

PulseObj=pul.PulsesCLS(inpdir+"Pulses.json")

StateRemDct=js.load(open(inpdir+"StateRemover.json","r"))
StateRemover=list( StateRemDct["SpecificRM"])+\
   [i for i in np.arange(StateRemDct["RangeRM"][0],StateRemDct["RangeRM"][1])] 
                  
                  
##############################################################
### para 1D:
LatticeDct= js.load(open(inpdir+"Lattice.json", 'r'))
asLatt=LatticeDct["asLatt"]
LatticeVecs=np.asarray(LatticeDct["LatticeVecs"])*asLatt
RecipVecs=np.asarray(LatticeDct["RecipVecs"])*2*np.pi/asLatt
### armamos un vector, por las dudas. 

### Lectura de matrices
### kpts 1D
kptLs0=np.loadtxt("Energs/BandStruct-ibd-0000.dat")
Nkpt=len(kptLs0)
kptLs1=np.zeros((Nkpt,3))
kptLs1[:,2]=kptLs0[:,0]



if (irank==0) :
  
  np.savetxt(outdir+"EnvPulse.dat" ,\
    np.transpose([PulseObj.tvec,PulseObj.pulsEnv]))


jk=np.zeros(PulseObj.Nt,dtype=float)
normie=np.zeros(PulseObj.Nt,dtype=float)
#####################################################

TLogFile=open(outdir+"Times-iproc-"+str(irank).zfill(4)+".dat","w")#,buffering=0)

### Loop en ikpt, paralelo.
for ik in np.arange(Nkpt):
  if (ik%nsize == irank) : 
    time2=time.time()
    Ebnd0=np.loadtxt("Energs/Ener-ikp-"+str(ik).zfill(4)+".dat")

    Omat0R=np.loadtxt("Matrices/Re-Ovrlp-"+str(ik).zfill(4))
    Omat0I=np.loadtxt("Matrices/Im-Ovrlp-"+str(ik).zfill(4))
    Dmat0R=np.loadtxt("Matrices/Re-Deriv-"+str(ik).zfill(4))
    Dmat0I=np.loadtxt("Matrices/Im-Deriv-"+str(ik).zfill(4))

    Omat0=Omat0R+1j*Omat0I
    Dmat0=Dmat0R+1j*Dmat0I
###############################################################
    ### poda de matrices
    Nbnd0=len(Ebnd0)
    ToRemove=np.less_equal(StateRemover,Nbnd0-1)
    nrem=np.sum(ToRemove) 
      
    Nbnd=len(Ebnd0)-nrem

    Ebnd=np.zeros(Nbnd ,dtype=complex)
    Omat=np.zeros((Nbnd,Nbnd),dtype=complex)
    Dmat=np.zeros((Nbnd,Nbnd),dtype=complex)
    Emat=np.zeros((Nbnd,Nbnd),dtype=complex)
    ivec=np.zeros(Nbnd,dtype=int)

    cT=np.zeros(Nbnd,dtype=complex)
    cT0=np.zeros(Nbnd,dtype=complex)
    qT0=np.zeros(Nbnd,dtype=complex)


    
    ibnd=0
    for ibnd0 in np.arange(Nbnd0):
      
      if ibnd0 in StateRemover:
        continue
      ivec[ibnd]=ibnd0
      Ebnd[ibnd]=Ebnd0[ibnd0]
      ibnd+=1

    np.savetxt(outdir+"Ebnd-"+str(ik).zfill(4)+".dat",np.real(Ebnd))

    cT0[0]=1.0        

    cT=cT0*np.exp(-1j*Ebnd*PulseObj.tvec[0] )  
    cT0*=np.exp(-1j*Ebnd*(PulseObj.tvec[0]+PulseObj.dt2) )  
    for ib1, ibnd1 in enumerate(ivec,0):
      for ib2,ibnd2 in enumerate(ivec,0): 
        Dmat[ib1,ib2]=Dmat0[ibnd1,ibnd2]
        Omat[ib1,ib2]=Omat0[ibnd1,ibnd2]
    for ib1, ibnd1 in enumerate(ivec,0):
#      Emat[:,ib1]=Ebnd[ibnd1]*Omat[:,ib1]
      Emat[:,ib1]=Ebnd[ib1]*Omat[:,ib1]
#    Ebnd=Ebnd0
    #for ibnd in 

#    OChU=scipy.linalg.lapack.zpotrf(Omat)
#    OChuInv=scipy.linalg.lapack.ztrtri(OChU)
#    OChuTInv=scipy.linalg.lapack.ztrtri(np.transpose(Omat))

    ### Invert Omat, calc new Omat and Dmat

    H0=Emat

    kp = np.dot(RecipVecs*kptLs1[ik,:] ,PulseObj.pol)

################################################################
    for it,t in enumerate(PulseObj.tvec[:-1],0):
      #pass

      Hmat=-1j*PulseObj.dt2*(H0+PulseObj.pulseA[it]*(-1j*Dmat+kp*Omat)) #*pulseA[it]*Dmat)
        ### -1j*dt2
      #Hmat=-1j*dt*(H0+0.5*(pulseA[it]+pulseA[it+1])*kp*Omat+(-1j)*0.5*(pulseA[it]+pulseA[it+1])*Dmat)
      qT0=np.dot(Hmat,cT0)
      cT=cT+2*qT0#np.dot(Hmat,cT0)#np.dot(expH,cT)

      #Hmat=-1j*PulseObj.dt2*(H0+0.5*(PulseObj.pulseA[it]+PulseObj.pulseA[it+1])*(kp*Omat-1j*Dmat))
      Hmat=-1j*PulseObj.dt2*(H0+(PulseObj.pulseAstg[it])*(kp*Omat-1j*Dmat))
        ### -1j*dt2
      qT0=np.dot(Hmat,cT)
      cT0=cT0+2*qT0#np.dot(Hmat,cT)
      normie[it]=np.vdot(cT,cT)
      jk[it]=-np.real( kp*np.vdot(cT, cT) \
         -1j*np.vdot(cT, Dmat.dot(cT) )) +PulseObj.pulseA[it] 
        
    ## Last step:
    Hmat=-1j*PulseObj.dt2*(H0+PulseObj.pulseA[-1]*(-1j*Dmat+kp*Omat)) #*pulseA[it]*Dmat)
    qT0=np.dot(Hmat,cT0)
    cT=cT+2*qT0#np.dot(Hmat,cT0)#np.dot(expH,cT)
    normie[-1]=np.vdot(cT,cT)
    jk[-1]=-np.real( kp*np.vdot(cT, cT) \
         -1j*np.vdot(cT, Dmat.dot(cT) )) +PulseObj.pulseA[-1] 


    np.savetxt(outdir+"cT-"+str(ik).zfill(4)+".dat",np.transpose([PulseObj.tvec,normie]))
    np.savetxt(outdir+"jt-"+str(ik).zfill(4)+".dat",np.transpose([PulseObj.tvec,jk]))



    time3=time.time()
    print("kp ", ik," finished, total time ", time3-time2)
    TLogFile.write("kp "+str(ik)+" finished, total time "+str( time3-time2)+"\n")
#np.savetxt("Normie.dat",normie[0:-1])
TLogFile.close()
comm.Barrier()
if irank==0:
  time4=time.time()
  print("Total running time: ", time4-time1)
  ### measure to catch a race condition.
  
  cmdstr="cat "+outdir+"Times-iproc-* > "+outdir+"TimesProc.dat"
  print(cmdstr)
  call(cmdstr,shell=True)
  np.savetxt(outdir+"Nt-step.out",[PulseObj.dt,PulseObj.Nt])

#comm.Finalize()
#MPI_Finalize()


