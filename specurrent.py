#!/usr/bin/python

import math
import numpy as np
import csv 
import scipy.integrate as spi
#import scipy.integrate as spi#.integrate as integ
import os
from subprocess import call
import subprocess
from os import listdir
from os.path import isfile, join, isdir
import fnmatch
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
#from scipy.signal import argrelextrema
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
#import colormaps as cmaps
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA

from src import pulses as pul
from src import basics as bas

eV_to_au=27.2113845;  angs_to_au=0.529177249
fs_to_au=0.02418884; wm2_to_au=3.51e+16 

inpdir="input/"
### en algun momento va a estar piola hacerlo leer el inp de fortran

HOmax=200

outdirLs=[f+"/" for f in listdir(".") if isdir(f) if "output_" in f]
PulseObj=pul.PulsesCLS(inpdir+"Pulses.json")






for outdir in outdirLs:
  
  methname=outdir.split("_")[-1][:-1]
  jdir=outdir
  jfileLs0 = [jdir+f for f in listdir(jdir) if isfile(join(jdir, f)) if  "jt-" in (f)  ]
  jfileLs=sorted(jfileLs0)
  print(jfileLs)
  ##filexist[ils]=os.path.isfile(filazo) 
  ##RecipVecs=np.loadtxt("RecipVecs.txt")
  ##subprocess.check_call("cp LatticeVecs.txt Vectors/.", shell=True)
  ##np.savetxt(matdire+"Re-Zdip-"+str(ikp).zfill(4) ,np.real(ZdipMat[0 ,:,:]) )

  
  
  env=PulseObj.pulsEnv#np.loadtxt(jdir+"EnvPulse.dat")[:,1]
  jtot=np.loadtxt(jfileLs[0])[:,1]*0.0
  tvec=PulseObj.tvec#np.loadtxt(jfileLs[0])[:,0]
  #print("POR AHORA PROCEDO CON UNA SOLA")
  #jtot=np.zeros_like(jtot)
  print(jtot.shape)
  for ij, ifile in enumerate(jfileLs,0):
  	jaux=np.loadtxt(jfileLs[ij])
  	
  	jtot+=jaux[:,1]/np.sqrt(len(jaux[:,1]))
  	
  ##jmask=np.zeros_like(jtot)
  
  ##np.savetxt( "output/jtot.dat",jtot)
  bas.exportvec(outdir+"jtot_"+methname+".dat", tvec,jtot )
  jtot=jtot*env ### MULTIP POR LA ENV
  average=sum(jtot)/jtot.shape[-1]
  jfreq=np.fft.fft(jtot)/jtot.shape[-1]
  nfreq=jfreq.shape[-1]
  
  ht=tvec[1]-tvec[0]
  
  freq = np.fft.fftfreq(tvec.shape[-1])*2.0*np.pi/ht/PulseObj.omegaIR
  #plt.plot(freq, np.log(pow( np.abs(jfreq),2) )) #, freq, sp.imag)
  #plt.savefig("jspec.png" ,bbox_inches='tight')
  #np.savetxt( "output/jfou.dat",np.log(pow( np.abs(jfreq),2) ) )
  bas.exportvec(outdir+"jspec.dat",freq[0:nfreq//2-1],np.log10(pow( np.abs(jfreq),2) )[0:nfreq//2-1])
  
  figuretti = plt.figure(figsize=(12.0, 8.0), dpi=160)
  ax1 = figuretti.add_subplot(111)
  
  ax1.set_xlabel('Harmonic order', fontsize = 28)
  ax1.set_ylabel('yield (arb. units)', fontsize = 28)
  
  ax1.plot(freq[0:nfreq//2-1],np.log10(pow( np.abs(jfreq),2) )[0:nfreq//2-1],color="b", \
   linestyle="-", linewidth=1) #,label="PE spectrum")
  #ax1.legend(fontsize = 28)
  
  ax1.set_xlim(0,HOmax)
  #ax1.set_xticks(np.arange(11, 29, 2.0))
  #ax1.set_ylim(0.0,1.0)
  
  #ax1.set_yticks(np.arange(0.0, 1.2, 0.2))
  
  for ikla in np.arange(0,HOmax):
  	if (ikla%2 ==1) :
  		plt.axvline(x=ikla, color='k', linestyle='-', linewidth=1)
  	if (ikla%2 ==0) :
  		plt.axvline(x=ikla, color='k', linestyle='--', linewidth=1)
  
  
  
  ax1.tick_params("both",labelsize=24)
  
  plt.savefig(outdir+"jspec_"+methname+".png" ,bbox_inches='tight')
  
  
  
  
  
  
