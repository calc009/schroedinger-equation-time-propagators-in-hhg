#!/usr/bin/python

import math
import numpy as np
import csv 
import scipy.integrate as spi
#import scipy.integrate as spi#.integrate as integ
import os
from subprocess import call
import subprocess
from os import listdir
from os.path import isfile, join, isdir
import fnmatch
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
#from scipy.signal import argrelextrema
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
#import colormaps as cmaps
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA

from src import pulses as pul
from src import basics as bas





linevec=["-","-","--","-.","-","-"]
colorvec=["k","r","g","b","y","c"]

inpdir="input/"
figudir="figures/"

iancho=1
HOmax=300

PulseObj=pul.PulsesCLS(inpdir+"Pulses.json")

#dirLs=["PyCheb","PyCN","PyRKsto45","PyRKsto23","PyEkitVec","PyLF"]
#customLabels=["Cheb.","CN","RK45","RK23","ME","LF"]

outdirLs=[f+"/" for f in listdir(".") if isdir(f) if "output_" in f]
#dirLs=["PyRKsto45","PyRKsto23","PyLF","PyCN","PyEkitVec"]#["PyCN","PyRKsto45","PyRKsto23","PyLF"]#["PyCN","PyRKstoErr-6","PyRKsto23","PyEkitVec","PyLF"]
#customLabels=["RK45","RK23","LF","CN","ME"]#["CN","RK45","RK23","LF"]#["CN","RK45","RK23","ME","LF"]
customlabels=[f.split("_")[-1][:-1] for f in outdirLs]

#jtot=np.zeros_like(jtot)

#kpts=np.loadtxt("Vectors/kpoints.dat")

kpts=np.loadtxt("Energs/BandStruct-ibd-0000.dat")[:,0]
print(kpts[0],kpts[-1])
igamma=bas.gammaest(kpts,0.0)
print("igamma =", igamma)

EnerFile=outdirLs[0]+"Ebnd-"+str(igamma).zfill(4)+".dat"
Enervec=np.loadtxt(EnerFile  ); nEn=len(Enervec)
Gaps=np.zeros(nEn-1)
TwoGaps=np.zeros(nEn-2)
for iEn in np.arange(nEn-1):
  Gaps[iEn]=Enervec[iEn+1]-Enervec[iEn]

for iEn in np.arange(nEn-2):
  TwoGaps[iEn]=Enervec[iEn+2]-Enervec[iEn]
UnitPhoton=PulseObj.omegaIR;

Gaps=Gaps/UnitPhoton
TwoGaps=TwoGaps/UnitPhoton

print(Gaps[0:5])

figuretti = plt.figure(figsize=(20, 10), dpi=280)
ax1 = figuretti.add_subplot(111)

ax1.set_xlabel('Harmonic order', fontsize = 18)
ax1.set_ylabel('Yield (arb. units)', fontsize = 18)

for idir, dire in enumerate(outdirLs,0):


  methname=dire.split("_")[-1][:-1]
  jfile=dire+"jspecGamma.dat"

  jfreq=np.loadtxt(jfile)[:,1]
  freq=np.loadtxt(jfile)[:,0]
  nfreq=len(freq)

  ax1.plot(freq[0:nfreq//2-1],jfreq[0:nfreq//2-1],color=colorvec[idir], \
   linestyle=linevec[idir], linewidth=1,label=customlabels[idir]) #,label="PE spectrum")
  ax1.legend(fontsize = 12)
  
  mxf=np.minimum(nfreq/8,10000)
  #np.savetxt(figudir+"jspecGamma_"+dire+".dat",np.transpose( [freq[0:mxf],jfreq[0:mxf ]]) )

ax1.set_xlim(0,HOmax)
#ax1.set_xticks(np.arange(11, 29, 2.0))
#ax1.set_ylim(0.0,1.0)
#ax1.set_yticks(np.arange(0.0, 1.2, 0.2))

for ikla in np.arange(0,HOmax):
  if (ikla%2 ==1) :
    plt.axvline(x=ikla, color='k', linestyle='-', linewidth=1)
  if (ikla%2 ==0) :
#    plt.axvline(x=ikla, color='k', linestyle='--', linewidth=1)
    pass


for iGap in np.arange(0,6):
#  plt.axvline(x=Gaps[iGap], color='r', linestyle='-', linewidth=2)
#  plt.axvline(x=TwoGaps[iGap], color='g', linestyle='--', linewidth=2)
  plt.vlines(x=Gaps[iGap], ymin=-90, ymax=30, color='r', linestyle='-.', linewidth=2, zorder=2)


ax1.tick_params("both",labelsize=24)



ax1.set_xlim(0.2,HOmax)
ax1.set_ylim(-50,-5)
plt.savefig(figudir+"jspecGammaComp.png" ,bbox_inches='tight')

ax1.set_xlim(0.2,30.0)
ax1.set_ylim(-25,0)
plt.savefig(figudir+"jspecGammaCompNear.png" ,bbox_inches='tight')

ax1.set_xlim(35.0,125.0)
ax1.set_ylim(-35,-20)
plt.savefig(figudir+"jspecGammaCompNear2.png" ,bbox_inches='tight')

ax1.set_xlim(90.0,110.0)
ax1.set_ylim(-35,-25)
plt.savefig(figudir+"jspecGammaCompNear3.png" ,bbox_inches='tight')
#####################################################
#####################################################

figuretti = plt.figure(figsize=(20, 10), dpi=280)
ax1 = figuretti.add_subplot(111)

ax1.set_xlabel('Harmonic order', fontsize = 18)
ax1.set_ylabel('Yield (arb. units)', fontsize = 18)

for idir, dire in enumerate(outdirLs,0):


  methname=dire.split("_")[-1][:-1]
  jfile=dire+"jspec.dat"

  jfreq=np.loadtxt(jfile)[:,1]
  freq=np.loadtxt(jfile)[:,0]
  nfreq=len(freq)

  ax1.plot(freq[0:nfreq//2-1],jfreq[0:nfreq//2-1],color=colorvec[idir], \
   linestyle=linevec[idir], linewidth=1,label=customlabels[idir]) #,label="PE spectrum")
  ax1.legend(fontsize = 12)
  
  mxf=np.minimum(nfreq/8,10000)
  #np.savetxt(figudir+"jspecGamma_"+dire+".dat",np.transpose( [freq[0:mxf],jfreq[0:mxf ]]) )

ax1.set_xlim(0,HOmax)
#ax1.set_xticks(np.arange(11, 29, 2.0))
#ax1.set_ylim(0.0,1.0)
#ax1.set_yticks(np.arange(0.0, 1.2, 0.2))

for ikla in np.arange(0,HOmax):
  if (ikla%2 ==1) :
    plt.axvline(x=ikla, color='k', linestyle='-', linewidth=1)
  if (ikla%2 ==0) :
#    plt.axvline(x=ikla, color='k', linestyle='--', linewidth=1)
    pass


for iGap in np.arange(0,6):
#  plt.axvline(x=Gaps[iGap], color='r', linestyle='-', linewidth=2)
#  plt.axvline(x=TwoGaps[iGap], color='g', linestyle='--', linewidth=2)
  plt.vlines(x=Gaps[iGap], ymin=-90, ymax=30, color='r', linestyle='-.', linewidth=2, zorder=2)


ax1.tick_params("both",labelsize=24)



ax1.set_xlim(0.2,HOmax)
ax1.set_ylim(-50,-5)
plt.savefig(figudir+"jspecComp.png" ,bbox_inches='tight')

ax1.set_xlim(0.2,30.0)
ax1.set_ylim(-25,0)
plt.savefig(figudir+"jspecCompNear.png" ,bbox_inches='tight')

ax1.set_xlim(35.0,125.0)
ax1.set_ylim(-25,-15)
plt.savefig(figudir+"jspecCompNear2.png" ,bbox_inches='tight')

ax1.set_xlim(90.0,110.0)
ax1.set_ylim(-25,-15)
plt.savefig(figudir+"jspecCompNear3.png" ,bbox_inches='tight')


#############################################################################
##### Prob Comp 

ikpsel=bas.gammaest(kpts,0.0)
plt.gcf().clear()
figuretti = plt.figure(figsize=(10, 5), dpi=280)
ax1 = figuretti.add_subplot(111)

ax1.set_xlabel('Time (a.u.)', fontsize = 18)
ax1.set_ylabel('log(|Prob-1|)', fontsize = 18)

for idir, dire in enumerate(outdirLs,0):


  jfile=dire+"cT-"+str(ikpsel).zfill(4)+".dat"

  jfreq=np.loadtxt(jfile)[:,1]
  freq=np.loadtxt(jfile)[:,0]
  nfreq=len(freq)

  ax1.plot(freq,np.log10(np.abs(jfreq-1.0)),color=colorvec[idir], \
   linestyle=linevec[idir], linewidth=2,label=customlabels[idir]) #,label="PE spectrum")
  ax1.legend(fontsize = 12)


#ax1.legend(fontsize = 28)

ax1.set_xlim(-15000.0,15000.0)
#ax1.set_xticks(np.arange(11, 29, 2.0))
#ax1.set_ylim(0.0,1.0)
#ax1.set_yticks(np.arange(0.0, 1.2, 0.2))



ax1.tick_params("both",labelsize=24)

plt.savefig(figudir+"ProbConservComp.png" ,bbox_inches='tight')

###############################################################################
###############################################################################

#nproc=4
Time=[]
Terr=[]
TPS=[] ## per step
TPSerr=[]

for idir, dire in enumerate(outdirLs,0):

  kpts=np.loadtxt("Energs/BandStruct-ibd-0000.dat")[:,0]
  nkp=len(kpts)

  filetaT=dire+"cT-"+str(ikpsel).zfill(4)+".dat"
  jvec=np.loadtxt(filetaT)
  nT=jvec.shape[0]
#  print(nT)

  parsum=0.0
  errcuad=0.0

  
  fileta=dire+"/"+"TimesProc.dat"
  fread=open(fileta,"r")
  for linea in fread:
    row=linea.split()
#    print(row[-1])
    parsum+=float(row[-1])
  aveT=parsum/nkp#/nproc
  Time.append(aveT)
  fread.close
  fread=open(fileta,"r")
  for linea in fread:
    row=linea.split()
#    print(row[-1])
    errcuad+=(float(row[-1])-aveT)**2
  Terr.append( np.sqrt( errcuad   )/nkp )


  TPS.append(Time[idir]/nT )
  TPSerr.append(Terr[idir]/nT)
  print(customlabels[idir])
  print(r"Time per kpoint : ,%8.2f, +/-, %8.2f" % ( Time[idir],  Terr[idir]))
  print(r"Time per kpoint per time-step : ,%8.2e, +/-, %8.2e" % ( TPS[idir],  TPSerr[idir]))

#fread2=open(file2,"r")
#for linea in fread2:
#  row = linea.split()
#  if linea.isspace():
#    continue
#  iord2.append(float(row[0]))
#  exptphas.append(float(row[1]))
#  expterr.append(float(row[2]))
#fread2.close



#####################################################
##### j(t) Comp 

ijsel=12
plt.gcf().clear()
figuretti = plt.figure(figsize=(10, 7.5), dpi=280)
ax1 = figuretti.add_subplot(111)

ax1.set_xlabel('Time (a.u.)', fontsize = 18)
ax1.set_ylabel(r'$j_k(t)$', fontsize = 18)

for idir, dire in enumerate(outdirLs,0):

  methname=dire.split("_")[-1][:-1]
  jfile="output_"+methname+"/jt-"+str(ijsel).zfill(4)+".dat"

  jfreq=np.loadtxt(jfile)[:,1]
  freq=np.loadtxt(jfile)[:,0]
  nfreq=len(freq)

  ax1.plot(freq,jfreq,color=colorvec[idir], \
   linestyle=linevec[idir], linewidth=1,label=customlabels[idir]) #,label="PE spectrum")
  ax1.legend(fontsize = 12)


#ax1.legend(fontsize = 28)

ax1.set_xlim(5000.0,15000.0)
ax1.set_ylim(0.625,0.646)
#ax1.set_xticks(np.arange(11, 29, 2.0))
#ax1.set_ylim(0.0,1.0)
#ax1.set_yticks(np.arange(0.0, 1.2, 0.2))



ax1.tick_params("both",labelsize=24)

plt.savefig(figudir+"JtComp.png" ,bbox_inches='tight')

###############################################################################


