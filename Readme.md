# Time-dependent Schrödinger equation propagators comparison

In this project we implemented six (originally five) time steppers to propagate the Schrödinger Equation wavefunction forwards in time. The system is acted upon by the application of a long infrared pulse. It is expected that the absorption of successive photons and a recollision with the parent substrate produces higher energy emissions in form of multiples of the incident photon energy, a process known as High Harmonic Generation. It was first studied on atoms in the 2000s, and from the mid-to-late 2010s, the interest translated to solids, where new properties were discovered, namely a better scaling of the harmonic order population with the laser intensity.  


A quick overview of the time steppers is present in the poster presentation here in the repository, and for the newly introduced one, the Chebyshev propagator, we refer the reader to Ref. [1]. These methods are applied to a spectral formulation, where the wavefunction, is proposed as a combination of the target system eigenstates, as opposed to spatial-grid discretizations which work with the values of the desired wavefunction at specific spatial locations. 

Starting with the different matrix operators that are expressed in terms of the eigenstate basis, 
we implemented the following MPI parallelized propagators: Leap-Frog (LF), Runge-Kutta 2(3) (RK23), Runge-Kutta 4(5) (RK45), Crank-Nicolson (CN), Magnus Expansion (ME) and Chebyshev approximation in order to calculate the time evolution of an electron in the solid. The electron current generated is the end result of the calculation, since the former is related to the radiation process that will lead to the High Harmonic Generation. Within the scope of this project we focused on monitoring the quality of the results and the methods' CPU-time consumption.  

The physics of the results was evaluated by the multi-plateau nature of the energy representation of the current (see poster results and the files under the figures directory), and that the droppoff to the subsquent plateau is coincident with a bandgap (shown as red vertical lines in the poster).

In order to keep the repository lightweight, the output files for each method were not uploaded, although the resulting figures were kept to display the different methods' quality.


## New run results

The codes were run with a newer computer than the one used at the time of making the poster presentation for the DAMOP conference in early 2019. Especially the consumed-time information, there are some changes. The Chebyshev method used about half the time as the Magnus Expansion, while delivering comparable results (see figures directory). 
The time consumed per kpoint and per kpoint and time step were as follows, keeping in mind that what is relevant is the relative performance. We did a 15-basis-state calculation as an example for the code. 

-LF
Time per kpoint : ,   18.11, +/-,     0.05
Time per kpoint per time-step : ,3.45e-05, +/-, 9.40e-08
-RK45
Time per kpoint : ,  124.82, +/-,     0.80
Time per kpoint per time-step : ,2.38e-04, +/-, 1.52e-06
-Cheb
Time per kpoint : ,   83.17, +/-,     0.44
Time per kpoint per time-step : ,1.59e-04, +/-, 8.44e-07
-RK32
Time per kpoint : ,   60.54, +/-,     0.74
Time per kpoint per time-step : ,1.15e-04, +/-, 1.42e-06
-CN
Time per kpoint : ,   16.59, +/-,     0.05
Time per kpoint per time-step : ,3.16e-05, +/-, 9.11e-08
-ME
Time per kpoint : ,  153.06, +/-,     0.46
Time per kpoint per time-step : ,2.92e-04, +/-, 8.79e-07

The Crank-Nicolson method performed surprisingly well, considering the 2019 results where it showed poor scaling with the matrix size. This can only be explained from a chage or better optimization of the lower level linear algebra methods implemented in SciPy. All in all, the best methods under the new light are the CN, Chebyshev, ME.

## Usage 

These codes were MPI parallelized, however, there is no interaction between the processors in either of the implementations. There is a script that will run all six methods in parallel with 8 processors (the number should be replaced according to the available real cores). After completion, the user should run the three specurrent*.py scripts, which generate the energy representation of the electron probability current. Finally, the script ComPlotJGamma.py gathers the currents, which are the physically meaninful magnitude, and place them side by side. 

The input is given through three json files, as their names suggest, related to a specific physical aspect: electromagnetic radiation pulse properties, crystalline lattice properties within the 1D model, and the choice of removing specific eigenstates from the calculation. 

The code used to generate the basis set and the operator matrices is not present in this repository, as it is a much more complex code written in Fortran, since the scope of the present project is to test the propagation methods. 

## References

1. J. Chem. Phys. **81**, 3967 (1984) https://doi.org/10.1063/1.448136


