
import numpy as np 
import scipy.interpolate as spl
import json as js
from . import basics as bsc
################ Pulse, tvec


class PulsesCLS():
    
    
    
  def __init__(self, jsonfile):
  
    self.get_all_pulsedata(jsonfile)
    
    self.t0=-self.Nt/2*self.dt;
    self.dt2=0.5*self.dt
    self.IRpower=self.IRpower/bsc.wm2_to_au; 
    self.IRamp=np.sqrt(self.IRpower)
    self.pol=self.pol/np.sqrt( np.dot(self.pol ,self.pol))
    self.omegaIR=self.omegaIR_eV/bsc.eV_to_au
    self.IRwidth=self.IRwidth_fs/bsc.fs_to_au
    self.alphaIR=(2.0*np.log(2.0)/self.IRwidth**2)
    self.pulseA=np.zeros(self.Nt,dtype=float)
    self.pulseAstg=np.zeros(self.Nt,dtype=float)
    
    
    self.tvec=np.linspace(self.t0,self.t0+self.Nt*self.dt,self.Nt)
    print("IRamp","omegaIR","IRwidth",\
          self.IRamp,self.omegaIR,self.IRwidth)

    self.pulseAstg[0:self.Nt]=-self.IRamp*np.exp(\
        -self.alphaIR*(self.tvec-self.tdelay+self.dt2)**2)* \
        np.sin(self.omegaIR*(self.tvec-self.tdelay\
        +self.dt2)+self.phiCEP)/self.omegaIR
          
    self.pulseA[0:self.Nt]=-self.IRamp*np.exp(\
            -self.alphaIR*(self.tvec-self.tdelay)**2)* \
            np.sin(self.omegaIR*(self.tvec-self.tdelay\
            )+self.phiCEP)/self.omegaIR
          
    #pulseA[Nt]=-IRamp*np.exp(-alphaIR*(tvec[Nt-1]+dt-tdelay+dt2)**2)* \
    #   np.sin(omegaIR*(tvec[Nt-1]+dt-tdelay+dt2)+phiCEP)/omegaIR
    self.pulsEnv=-self.IRamp*np.exp(-self.alphaIR*\
    (self.tvec-self.tdelay)**2)/self.omegaIR 


    self.pulseAinterp=spl.CubicSpline(self.tvec,self.pulseA)

  def pulsefun(self,tp):
    y=-self.IRamp*np.exp(\
        -self.alphaIR*(tp-self.tdelay)**2)* \
        np.sin(self.omegaIR*(tp-self.tdelay)\
               +self.phiCEP)/self.omegaIR
    return y
  
  def get_all_pulsedata(self,jsonfile):
    PulseData = js.load(open(jsonfile, 'r'))
    self.dt=PulseData["dt"]
    self.Nt=PulseData["Nt"]
    self.ievery=PulseData["ievery"]
    self.tdelay=PulseData["tdelay"]
    self.IRpower=PulseData["IRpower"]
    self.pol=PulseData["pol"]
    self.phiCEP=PulseData["phiCEP"]
    self.omegaIR_eV=PulseData["omegaIR_eV"]
    self.IRwidth_fs=PulseData["IRwidth_fs"]
    
      
      
      
      
      
        