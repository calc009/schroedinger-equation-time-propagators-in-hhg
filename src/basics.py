
import numpy as np


eV_to_au=27.2113845;  angs_to_au=0.529177249
fs_to_au=0.02418884; wm2_to_au=3.51e+16 
mu=1.0;

def simplecount(filename):
  lines=0
  for line in open(filename):
    lines+=1
  return lines

def file_len(fname):
  ii=0
  with open(fname,"r") as f:
    for ii, l in enumerate(f):
      #print ii
      ii=ii+1
  return ii+1

def line_count(filename):
  count=0
  
  with open (filename,'r') as f:
    print( "opened file:" , filename)
    for line in f:
      count+=1
  #print "count", count
  return count


def column(matrix, i):
    return [row[i] for row in matrix]


def fitfuncExpDec(tau,lam,y0,x0):
  return (y0+np.exp(-lam*(tau-x0)))

def exportvec(filename,xvec,yvec):

  if len(xvec)==len(yvec):
    fileout=open(filename,"w")  
    for ix, x in enumerate(xvec,0):  
      fileout.write(str(x)+" "+str(yvec[ix])+"\n")
    fileout.close
  else :
    print ("xvec, yvec have different lengths")
    print ("len(xvec)", len(xvec)," len(yvec)", len(yvec))
#  fout.write( str( numlist[inum])+" "+str(abs(fitParams[5])/abs(fitParams[2]))+"\n" )

def linearfit(x,A,B):
  return A*x+B

def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)



def infrared(ener) :
  valpiso=9.0
  val=0.03297569+np.exp(-0.25230764*(ener-13.54354019))
  if ener > valpiso :
    val=0.03297569+np.exp(-0.25230764*(ener-13.54354019))
  else :
    val=0.03297569+np.exp(-0.25230764*(valpiso-13.54354019))
  return val

def elimrep(seq): 
   # order preserving
   checked = []
   for e in seq:
       if e not in checked:
           checked.append(e)
   return checked

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]

def find_nearestI(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx 

def roundcyph(x,n):
  y=round( x*10**n,10**n)/10**n
  return y

def directory_check_create(dirname,clear=True):
  import subprocess as sbp
  from os.path import isfile, isdir
  existFILE=isfile(dirname)
  if existFILE:
    print (dirname+" exists as a file, not a directory!")
    return 
  existsDIR=isdir(dirname)
  if existsDIR==True:
    if clear==True:
      sbp.call( "rm -rf "+dirname ,shell=True)
      sbp.call( "mkdir "+dirname ,shell=True)
  else :  
    sbp.call( "mkdir "+dirname ,shell=True)
  
  return

def gammaest(kvec,k0):

  idx=np.argmin(abs(kvec-k0))
  
  return idx