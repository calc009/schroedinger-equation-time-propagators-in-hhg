import os
import subprocess as sbp
from os import listdir
from os.path import isfile, join, isdir


runLs=[f for f in listdir(".") if isfile(f) if "run" in f if ".py" in f]

for run in runLs:
  name=run.split("_")[-1][:-1]  
  cmdstr=" mpiexec -np 8 --bind-to core python "+run ##
  print(cmdstr)
  sbp.call(cmdstr,shell=True)
